package com.desire.service;

import java.util.List;

import com.desire.entity.UserEntity;

public interface IUserService {

	UserEntity findById(int id);

	List<UserEntity> findAll();

	int insertUser(UserEntity userEntity);

	int updateUser(UserEntity userEntity);

	int deleteUserById(int id);

}