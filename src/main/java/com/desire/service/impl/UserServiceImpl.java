package com.desire.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.desire.entity.UserEntity;
import com.desire.mapper.IUserMapper;
import com.desire.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserMapper userMapper;

	@Override
	public UserEntity findById(int id) {
		// TODO Auto-generated method stub
		return userMapper.findById(id);
	}

	@Override
	public List<UserEntity> findAll() {
		// TODO Auto-generated method stub
		return userMapper.findAll();
	}

	@Override
	public int insertUser(UserEntity userEntity) {
		// TODO Auto-generated method stub
		return userMapper.insertUser(userEntity);
	}

	@Override
	public int updateUser(UserEntity userEntity) {
		// TODO Auto-generated method stub
		return userMapper.updateUser(userEntity);
	}

	@Override
	public int deleteUserById(int id) {
		// TODO Auto-generated method stub
		return userMapper.deleteUserById(id);
	}

}
