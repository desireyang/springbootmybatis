package com.desire.service.impl;

import com.desire.entity.SysUser;
import com.desire.mapper.ISysUserMapper;
import com.desire.service.ISysUserService;
import com.desire.util.ReadExcelSysUserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class SysUserServiceImpl implements ISysUserService {

    @Autowired
    private ISysUserMapper sysUserMapper;
    @Override
    public void insertByForeachTag(List<SysUser> sysUserList) {
        sysUserMapper.insertByForeachTag(sysUserList);
    }
}
