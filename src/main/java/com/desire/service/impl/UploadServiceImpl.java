package com.desire.service.impl;

import com.desire.entity.SysUser;
import com.desire.mapper.ISysUserMapper;
import com.desire.service.IUploadService;
import com.desire.util.ReadExcelSysUserUtil;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.util.List;

@Service
public class UploadServiceImpl implements IUploadService {

    @Autowired
    private ISysUserMapper sysUserMapper;

    @Override
    public boolean batchImport(String name, MultipartFile file) {
        boolean b = false;
        // 创建处理Excel
        ReadExcelSysUserUtil readExcel = new ReadExcelSysUserUtil();
        // 解析Excel，获取书本信息集合
        List<SysUser> sysUserList = readExcel.getExcelRead(name, file);
        for (SysUser sysUser : sysUserList) {
            System.err.println(sysUser.getName());
        }
        if (sysUserList != null) {
            b = true;
        }
        // 直接将list集合作为参数，在MyBatis的相应映射文件中使用foreach标签进行批量添加
        sysUserMapper.insertByForeachTag(sysUserList);
        return b;
    }

    @Override
    public void export(String[] titles, ServletOutputStream out) {
        try {
            // 第一步，创建一个workbook，对应一个Excel文件
            HSSFWorkbook workbook = new HSSFWorkbook();
            // 第二步，在weorkbook中添加一个sheet，对应Excel文件中的sheet
            HSSFSheet sheet = workbook.createSheet("sheet1");
            // 第三步，在sheet中添加表头第0行，注意老版本POI对Excel的行数列数有限制short
            HSSFRow row = sheet.createRow(0);
            // 第四步，创建单元格，并设置值表头，设置表头居中
            HSSFCellStyle hssfCellStyle = workbook.createCellStyle();
            // 居中样式
            hssfCellStyle.setAlignment(HorizontalAlignment.CENTER);
            HSSFCell hssfCell = null;
            for (int i = 0; i < titles.length; i++) {
                hssfCell = row.createCell(i);// 列索引从0开始
                hssfCell.setCellValue(titles[i]);// 列名1
                hssfCell.setCellStyle(hssfCellStyle);// 列居中显示
            }
            // 第五步，将文件输出到客户端浏览器
            try {
                workbook.write(out);
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
