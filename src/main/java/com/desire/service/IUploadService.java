package com.desire.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;

public interface IUploadService {
    boolean batchImport(String name, MultipartFile file);

    //模板
    void export(String[] titles, ServletOutputStream out);
}
