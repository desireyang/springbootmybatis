package com.desire.controller;

import java.util.List;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.desire.entity.UserEntity;
import com.desire.service.IUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@RestController
@RequestMapping("/testBoot")
public class UserController {

	@Autowired
	private IUserService userService;

	@RequestMapping("getUser")
	public String getUser(@RequestParam("id") int id) {
		UserEntity userEntity = userService.findById(id);
		String result = "";
		if (userEntity != null) {
			result = JSON.toJSONString(userEntity);
		} else {
			result = "没有id为" + id + " 的用户！";
		}
		return result;
	}

	@RequestMapping("getUsers")
	public String getUsers(@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<UserEntity> list = userService.findAll();
		// 得到分页的结果对象
		PageInfo<UserEntity> pageInfo = new PageInfo<>(list);
		System.out.println("当前页码:" + pageInfo.getPageNum());
		System.out.println("总页码:" + pageInfo.getPages());
		System.out.println("总记录数:" + pageInfo.getTotal());
		System.out.println("在页面需要连续显示的页码");
		int[] nums = pageInfo.getNavigatepageNums();
		for (int i : nums) {
			System.out.print(" " + i);
		}
		// 得到分页中的User条目对象
		List<UserEntity> users = pageInfo.getList();
		String s = JSON.toJSONString(users);
		return s;
	}

	@RequestMapping("addUser")
	public String addUser(@ModelAttribute UserEntity userEntity) {
		int i = userService.insertUser(userEntity);
		String result = "";
		if (i > 0) {
			result = "inster User SUCCESS!!! ID: " + userEntity.getId();
		} else {
			result = "inster User FAIL！！！";
		}
		return result;
	}

	@RequestMapping("updateUser")
	public String updateUser(@ModelAttribute UserEntity userEntity) {
		int i = userService.updateUser(userEntity);
		String result = "";
		if (i > 0) {
			result = "update id=" + userEntity.getId() + " User SUCCESS!!!";
		} else {
			result = "update id=" + userEntity.getId() + " User FAIL！！！";
		}
		return result;
	}

	@RequestMapping("deleteUser")
	public String deleteUser(@RequestParam("id") int id) {

		int i = userService.deleteUserById(id);
		String result = "";
		if (i > 0) {
			result = "delete id=" + id + " User SUCCESS!!!";
		} else {
			result = "delete id=" + id + " User FAIL！！！";
		}
		return result;
	}
}