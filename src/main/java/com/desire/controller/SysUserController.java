package com.desire.controller;

import com.desire.service.ISysUserService;
import com.desire.service.IUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@RestController
@RequestMapping("/pl")
public class SysUserController {
    @Autowired
    private IUploadService uploadService;

    @RequestMapping(value="/download_excel")
    public String down(HttpServletResponse response,
                       @RequestParam("name")String name) throws UnsupportedEncodingException {
        response.setContentType("application/binary;charset=UTF-8");
        try {
            ServletOutputStream out = response.getOutputStream();
            //设置文件头：最后一个参数是设置下载文件名
            response.setHeader("Content-Disposition", "attachment;fileName="+ URLEncoder.encode(name+".xls","UTF-8" ));
            String[] titles = {"用户名","密码","姓名","职务","登录方式（1：App 2：WEB）","身份证ID","性别（0：男1：女）","手机号","角色","部门"};
            uploadService.export(titles, out);
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "导出信息失败";
        }
    }

    @RequestMapping(value = "batchimport", method = RequestMethod.POST)
    public String batchimport(@RequestParam("file") MultipartFile file, HttpServletRequest request,
                              HttpServletResponse response) {
        try {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String msg = "";
        // 判断文件是否为空
        if (file == null) {
            return null;
        }
        // 获取文件名
        String fileName = file.getOriginalFilename();
        // 进一步判断文件是否为空（即判断其大小是否为0或其名称是否为null）
        long size = file.getSize();
        if (fileName == null || ("").equals(fileName) && size == 0) {
            return null;
        }
        // 批量导入。参数：文件名，文件
        boolean b = uploadService.batchImport(fileName, file);
        if (b) {
            msg = "import success!";
//            request.getSession().setAttribute("msg", msg);
        } else {
            msg = "import failed!";
//            request.getSession().setAttribute("msg", msg);
        }
        return msg;
    }
}
