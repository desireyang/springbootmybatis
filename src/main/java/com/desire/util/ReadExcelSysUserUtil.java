package com.desire.util;

import com.desire.entity.SysUser;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReadExcelSysUserUtil {
    //总行数
    private int totalRows = 0;
    //总条数
    private int totalCells = 0;
    //错误信息接收器
    private String errorMsg;

    public int getTotalRows() {
        return totalRows;
    }

    public int getTotalCells() {
        return totalCells;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * 验证Excel文件
     *
     * @param filePath
     * @return
     */
    public boolean validateExcel(String filePath) {
        if (filePath == null || !(WDWUtil.isExcel2003(filePath) || WDWUtil.isExcel2007(filePath))) {
            errorMsg = "文件名不是Excel格式";
            return false;
        }
        return true;
    }

    /**
     * 读取Excel文件，获取书本信息集合
     *
     * @param fileName
     * @param mFile
     * @return
     */
    public List<SysUser> getExcelRead(String fileName, MultipartFile mFile) {

        //文件名
        fileName = mFile.getOriginalFilename();
        // 后缀名
        String suffxName = fileName.substring(fileName.lastIndexOf("."));
        // 上传后的路径
        String filePath = "D://fileupload//";
        // 新建一个文件
        String newFileName = System.currentTimeMillis() + suffxName;
        File dest = new File(filePath + newFileName);
        // 创建一个目录（它的路径名由当前File对象指定，包括任一必须的父路径）
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        // 将上传的文件写入新建的文件中
        try {
            mFile.transferTo(dest);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        // 初始化信息的集合
        List<SysUser> list = new ArrayList<>();
        // 初始化输入流
        InputStream is = null;
        try {
            // 验证文件名是否合格
            if (!validateExcel(fileName)) {
                return null;
            }
            // 根据文件名判断文件时2003版本还是2007版本
            boolean isExcel2003 = true;
            if (WDWUtil.isExcel2007(fileName)) {
                isExcel2003 = false;
            }
            // 根据新建的文件实例化输入流
            is = new FileInputStream(dest);
            // 根据Excel里面的内容读取书本信息
            list = getExcelInfo(is, isExcel2003);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    /**
     * 根据Excel里面的内容读取信息
     *
     * @param is          输入流
     * @param isExcel2003 判断Excel版本是2003还是2007
     * @return
     * @throws IOException
     */
    private List<SysUser> getExcelInfo(InputStream is, boolean isExcel2003) {
        List<SysUser> list = null;
        try {
            //根据版本选择创建WorkBook的方式
            Workbook wb = null;
            //当Excel为2003时
            if (isExcel2003) {
                wb = new HSSFWorkbook(is);
            } else {
                //当Excel是2007时
                wb = new XSSFWorkbook(is);
            }
            //读取Excel里面的信息
            list = readExcelValue(wb);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 读取Excel里面的信息
     *
     * @param wb
     * @return
     */
    private List<SysUser> readExcelValue(Workbook wb) {
        //得到第一个Sheet
        Sheet sheet = wb.getSheetAt(0);

        //得到Excel的行数
        this.totalRows = sheet.getPhysicalNumberOfRows();

        //得到Excel的列数（前提是有行数）
        if (totalRows >= 1 && sheet.getRow(0) != null) {
            this.totalCells = sheet.getRow(0).getPhysicalNumberOfCells();
        }
        List<SysUser> list = new ArrayList<>();
        SysUser sysUser = null;
        //循环Excel行数，从第二行开始。标题不入库
        System.out.println("列数："+this.totalRows);
        for (int r = 1; r < this.totalRows; r++) {
            Row row = sheet.getRow(r);
            if (row == null) {
                continue;
            }
            //TODO：bean类实例化
            sysUser = new SysUser();
            //循环Excel的列
            System.err.println("行数："+this.totalCells);
            for (int c = 0; c < this.totalCells; c++) {
                Cell cell = row.getCell(c);
                if (cell != null) {
                    //全部设置成String
                    cell.setCellType(CellType.STRING);
                    if (c == 0) {
                        //用户名
                        String name = cell.getStringCellValue();
                        System.err.println(name);
                        sysUser.setName(name);
                    } else if (c == 1) {
                        //密码
                        String password = cell.getStringCellValue();
                        System.err.println(password);
                        sysUser.setPassword(password);
                    } else if (c == 2) {
                        //姓名
                        String realName = cell.getStringCellValue();
                        System.err.println(realName);
                        sysUser.setRealName(realName);
                    } else if (c == 3) {
                        //职务
                        String job = cell.getStringCellValue();
                        System.err.println(job);
                        sysUser.setJob(job);
                    } else if (c == 4) {
                        //登陆方式
                        int loginWay = Integer.parseInt(cell.getStringCellValue());
                        System.err.println(loginWay);
                        sysUser.setLoginWay(loginWay);
                    } else if (c == 5) {
                        //身份证ID
                        String idCard = cell.getStringCellValue();
                        System.err.println(idCard);
                        sysUser.setIdCard(idCard);
                    } else if (c == 6) {
                        //性别
                        int userSex = Integer.parseInt(cell.getStringCellValue());
                        System.err.println(userSex);
                        sysUser.setUserSex(userSex);

                    } else if (c == 7) {
                        //手机号
                        String userPhone = cell.getStringCellValue();
                        System.err.println(userPhone);
                        sysUser.setUserPhone(userPhone);

                    } else if (c == 8) {
                        //角色
//                        String job = cell.getStringCellValue();
                        System.err.println("角色");
//                        sysUser.setJob(job);
                    } else if (c == 9) {
                        //部门
                        String deptno = cell.getStringCellValue();
                        System.err.println(deptno);
                        sysUser.setDeptno(deptno);
                    }
                }
            }
            list.add(sysUser);
        }
        return list;
    }


}