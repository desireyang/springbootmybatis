package com.desire.util;

/**
 * 用于判断Excel版本
 */
public class WDWUtil {

    /**
     * 描述：是否是2003的Excel
     *
     * @param filePath
     * @return true是2003
     */
    public static boolean isExcel2003(String filePath) {
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    /**
     * 描述：是否是2007的Excel
     *
     * @param filePath
     * @return true是2007
     */
    public static boolean isExcel2007(String filePath) {
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }
}
