package com.desire.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.desire.entity.UserEntity;

@Repository
public interface IUserMapper {

	/**
	 * 根据id查找
	 * 
	 * @param id
	 * @return
	 */
	UserEntity findById(int id);

	/**
	 * 查找全部
	 * 
	 * @return
	 */
	List<UserEntity> findAll();

	/**
	 * 添加
	 * 
	 * @param userEntity
	 * @return
	 */
	int insertUser(UserEntity userEntity);

	/**
	 * 修改
	 * 
	 * @param userEntity
	 * @return
	 */
	int updateUser(UserEntity userEntity);

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	int deleteUserById(int id);
}