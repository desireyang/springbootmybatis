package com.desire.mapper;

import com.desire.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ISysUserMapper {

    /**
     * 批量添加
     *
     * @param sysUserList
     */
    void insertByForeachTag(@Param("sysUserList") List<SysUser> sysUserList);
}
