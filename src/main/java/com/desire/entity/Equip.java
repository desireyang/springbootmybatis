package com.desire.entity;

import java.io.Serializable;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
public class Equip implements Serializable {
    // 主键自动增长ID
    private Integer id;

    // 设备唯一标识号
    private String equipId;

    // USERID对应主表主键ID
    private String userId;

    // 用户登陆名
    private String name;

    // 用户真实姓名
    private String realName;

    // 用户部门
    private Integer deptno;

    // 设备IMEI
    private String im;

    private Date updateTime;

    private Date createTime;

    private String newUserId;

    private SysUser sysUser;

    // 设备类型：0：4G执法仪设备；1：无人机；2：智能眼镜；3：平板笔记本
    private Integer equipType;

    //声音（0关闭，1打开）
    private Boolean voiceStatus;
    //对讲（0关闭，1打开）
    private Boolean intercomStatus;
    //集群对讲（0关闭，1打开）
    private Boolean clusterIntercomStatus;
    //录像（0关闭，1打开）
    private Boolean videotapeStatus;
    //sos报警录像（0关闭，1打开）
    private Boolean sosVideotapeStatus;

    // (废弃2019年1月4日15:02:18)设备状态：0：空闲；1：对讲；2：录像；3：直播
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEquipId() {
        return equipId;
    }

    public void setEquipId(String equipId) {
        this.equipId = equipId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getDeptno() {
        return deptno;
    }

    public void setDeptno(Integer deptno) {
        this.deptno = deptno;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getNewUserId() {
        return newUserId;
    }

    public void setNewUserId(String newUserId) {
        this.newUserId = newUserId;
    }

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public Integer getEquipType() {
        return equipType;
    }

    public void setEquipType(Integer equipType) {
        this.equipType = equipType;
    }

    public Boolean getVoiceStatus() {
        return voiceStatus;
    }

    public void setVoiceStatus(Boolean voiceStatus) {
        this.voiceStatus = voiceStatus;
    }

    public Boolean getIntercomStatus() {
        return intercomStatus;
    }

    public void setIntercomStatus(Boolean intercomStatus) {
        this.intercomStatus = intercomStatus;
    }

    public Boolean getClusterIntercomStatus() {
        return clusterIntercomStatus;
    }

    public void setClusterIntercomStatus(Boolean clusterIntercomStatus) {
        this.clusterIntercomStatus = clusterIntercomStatus;
    }

    public Boolean getVideotapeStatus() {
        return videotapeStatus;
    }

    public void setVideotapeStatus(Boolean videotapeStatus) {
        this.videotapeStatus = videotapeStatus;
    }

    public Boolean getSosVideotapeStatus() {
        return sosVideotapeStatus;
    }

    public void setSosVideotapeStatus(Boolean sosVideotapeStatus) {
        this.sosVideotapeStatus = sosVideotapeStatus;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
