package com.desire.entity;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;



import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * SysUser entity. 系统用户表
 *
 * @author FYL
 * @version 1.0
 */
public class SysUser implements Serializable {

	public static final int STATUS_LOCK = 1;

	private static final long serialVersionUID = 1L;

	private Integer userId;
	private Integer parentid;
	private String password;
	private String name;
	private String realName;
	private Dept dept;

	private Emp emp;
	private String empnos;
	private String names;
	private String job;// 职务
//证件类型0：身份证；1：护照；2：社保
	private String documentType;

	/**
	 * 用户状态：0启用；1禁用；2删除
	 */
	private Integer status;

	/**
	 * 用户身份证号码
	 */
	private String idCard;

	/**
	 * 用户性别
	 */
	private Integer userSex;

	/**
	 * 用户图像
	 */
	private String userIcon;

	/**
	 * 用户手机号码
	 */
	private String userPhone;
	/**
	 * 部门ID
	 */
	private String deptno;

	private String dname;

	private String equipId;

	private String im;

	private String longitude; // 设备经度

	private String latitude; // 设备纬度

	private Integer country; // 发送定位位置（1国内，0国外，2未知）

	private String electricity; // 设备电量

	private String signalValue; // 设备信号

	private String positioningTime; // 接收定位的时间

	private String locationTime; // 发送定位的时间

	private String memory; // 内存
	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 用户所属登陆端
	 */
	private Integer entry;

	/**
	 * 登陆状态
	 */
	private Integer loginWay;

	/**
	 * @return loginWay
	 */
	public Integer getLoginWay() {
		return loginWay;
	}

	/**
	 * @param loginWay
	 *            要设置的 loginWay
	 */
	public void setLoginWay(Integer loginWay) {
		this.loginWay = loginWay;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	private Set<SysRole> sysRoles = new HashSet<SysRole>(0);

	private Set<Dept> depts = new HashSet<Dept>(0);

	private Set<RealTimePosition> realTimePositions = new HashSet<RealTimePosition>(
			0);

	// Constructors

	/** default constructor */
	public SysUser() {
	}

	/** minimal constructor */
	public SysUser(String name, String password) {
		this.name = name;
		this.password = password;
	}

	/** full constructor */

	public SysUser(String name, String password, Integer status,
                   Set<SysRole> sysRoles) {
		super();
		this.name = name;
		this.password = password;
		this.status = status;
		this.sysRoles = sysRoles;
	}

	public SysUser(Integer userId, String password, String name, Integer entry,
                   String realName, Integer status, String idCard, Integer userSex,
                   String userIcon, String userPhone, String deptno, Date createTime,
                   String job, Set<Dept> depts, Set<SysRole> sysRoles) {
		super();
		this.userId = userId;
		this.password = password;
		this.name = name;
		this.entry = entry;
		this.realName = realName;
		this.status = status;
		this.idCard = idCard;
		this.userSex = userSex;
		this.userIcon = userIcon;
		this.userPhone = userPhone;
		this.deptno = deptno;
		this.createTime = createTime;
		this.depts = depts;
		this.job = job;
		this.sysRoles = sysRoles;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Integer getEntry() {
		return entry;
	}

	public void setEntry(Integer entry) {
		this.entry = entry;
	}

	public Integer getUserSex() {
		return userSex;
	}

	public void setUserSex(Integer userSex) {
		this.userSex = userSex;
	}

	public String getUserIcon() {
		return userIcon;
	}

	public void setUserIcon(String userIcon) {
		this.userIcon = userIcon;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getDeptno() {
		return deptno;
	}

	public void setDeptno(String deptno) {
		this.deptno = deptno;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public static int getStatusLock() {
		return STATUS_LOCK;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Set<SysRole> getSysRoles() {
		return this.sysRoles;
	}

	public void setSysRoles(Set<SysRole> sysRoles) {
		this.sysRoles = sysRoles;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getDname() {
		return dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public SysUser(String password, String name, String realName,
                   Integer status, Set<SysRole> sysRoles) {
		super();
		this.password = password;
		this.name = name;
		this.realName = realName;
		this.status = status;
		this.sysRoles = sysRoles;
	}

	public SysUser(Integer userId, String password, String name,
                   String realName, Integer status, Set<SysRole> sysRoles) {
		super();
		this.userId = userId;
		this.password = password;
		this.name = name;
		this.realName = realName;
		this.status = status;
		this.sysRoles = sysRoles;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result
				+ ((realName == null) ? 0 : realName.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		SysUser other = (SysUser) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (realName == null) {
			if (other.realName != null){
				return false;
			}
		} else if (!realName.equals(other.realName)){
			return false;
		}
		if (status == null) {
			if (other.status != null) {
				return false;
			}
		} else if (!status.equals(other.status)){
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)){
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SysUser [userId=" + userId + ", password=" + password
				+ ", name=" + name + ", realName=" + realName + ", status="
				+ status + ",idCard=" + idCard + ", userSex=" + userSex
				+ ", userIcon=" + userIcon + ", userPhone=" + userPhone
				+ ", deptno=" + deptno + ", createTime=" + createTime + ",job="
				+ job + ", entry=" + entry + ", sysRoles=" + sysRoles
				+ ", depts=" + depts + "]";
	}

	/**
	 * @return realTimePosition
	 */
	public Set<RealTimePosition> getRealTimePosition() {
		return realTimePositions;
	}

	/**
	 * @param realTimePosition
	 *            要设置的 realTimePosition
	 */
	public void setRealTimePosition(Set<RealTimePosition> realTimePosition) {
		realTimePositions = realTimePosition;
	}

	public String getEquipId() {
		return equipId;
	}

	public void setEquipId(String equipId) {
		this.equipId = equipId;
	}

	public String getElectricity() {
		return electricity;
	}

	public void setElectricity(String electricity) {
		this.electricity = electricity;
	}

	public String getSignalValue() {
		return signalValue;
	}

	public void setSignalValue(String signalValue) {
		this.signalValue = signalValue;
	}

	public String getPositioningTime() {
		return positioningTime;
	}

	public void setPositioningTime(String positioningTime) {
		this.positioningTime = positioningTime;
	}

	public String getLocationTime() {
		return locationTime;
	}

	public void setLocationTime(String locationTime) {
		this.locationTime = locationTime;
	}

	public String getMemory() {
		return memory;
	}

	public void setMemory(String memory) {
		this.memory = memory;
	}

	public String getNames() {
		return names;
	}

	public void setNames(String names) {
		this.names = names;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public Emp getEmp() {
		return emp;
	}

	public void setEmp(Emp emp) {
		this.emp = emp;
	}

	public Dept getDept() {
		return dept;
	}

	public void setDept(Dept dept) {
		this.dept = dept;
	}

	public String getEmpnos() {
		return empnos;
	}

	public void setEmpnos(String empnos) {
		this.empnos = empnos;
	}

	public String getIm() {
		return im;
	}

	public void setIm(String im) {
		this.im = im;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Integer getCountry() {
		return country;
	}

	public void setCountry(Integer country) {
		this.country = country;
	}
}
